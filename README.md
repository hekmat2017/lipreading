
### Installation

To install all dependencies run the following command:

```
pip install -r requirements.txt

```

If you do not plan to use TensorFlow or TensorFlow GPU, remember to comment out and replace the line `tensorflow-gpu==1.10.0` with your Keras back-end of choice.

## Usage

### Preprocessing

To Preprocess videos in GRID directory run the following command:

```
python extract.py -v GRID -o data\dataset
```

Note: All align data must copy in GRID\alig

### Training

The training is configured to use multiprocessing with 2 workers by default.
Before starting the training, the dataset in the given directory is split by 20% for validation and 80% for training. A cache file of this split is saved inside the `data` directory, i.e: `data/dataset.cache`.

```
python train.py -d data/dataset -a data/aligns -e 2
```

### Evaluating

Use the `predict.py` script to analyze a video or a directory of videos with a trained model:

```
python predict.py -w data/res/2020-12-08-16-18/lipnet_065_1.96.hdf5 -v data/dataset_eval
```


